#pragma once

#include <functional>
#include <utility>
#include <vector>

namespace yackonop {

  typedef std::uint16_t              sz_t;
  typedef std::uint32_t              count_t;
  typedef std::vector<sz_t>          Sizes;
  typedef std::vector<count_t>       Counts;
  typedef std::vector<sz_t>          Strategy_t;
  typedef std::vector<Strategy_t>    StrategyList_t;

}
