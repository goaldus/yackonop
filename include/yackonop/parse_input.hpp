# pragma once 

#include <yackonop/types.hpp>

namespace yackonop {
  std::tuple<sz_t, Sizes, Counts> parse_input(
    std::istream& strm);
}
