#pragma once

#include <yackonop/types.hpp>
#include <yackonop/memoize.hpp>

namespace yackonop {

  struct Solver {
    const sz_t z;               // bag size
    const Sizes S;              // size of artices in
                                // each class in heap.
    const Counts c;             // article-counts for
                                // each class in heap.
    const StrategyList_t& G;    // strategies
    
    static const count_t INFTY;

    Solver(const sz_t z,
           const Sizes S,
           const Counts c,
           const StrategyList_t& G);

    tuple<count_t, Counts>
    solve();

    tuple<count_t, Counts>
    _min_vacancy(Counts c, count_t k);

    tuple<count_t, Counts>
    _vacancy(Counts c, count_t k, count_t i);

    function<
      tuple<count_t, Counts>(Counts , count_t)
      > memoized_min_vacancy;

    function<
      tuple<count_t, Counts>(Counts , count_t, count_t)
      > memoized_vacancy;
  };

}  // yackonop

// Local Variables:
// comment-auto-fill-only-comments: t
// End:
