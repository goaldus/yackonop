/**
 * Given bag size `z', and list of article sizes `s_i',
 * compute strategies `G_j' such that
 * `residual < min(s_i); residual = z - sum(G_j)'
 *
 */

#pragma once

#include <functional>

#include <yackonop/types.hpp>

namespace yackonop {
  struct Strategies {

    enum {
      FULL_SIZE = -1            // Flag for signatures
    };

    static const Strategy_t G0;

    sz_t z;                    // bag size
    const Sizes& sL;           // list of article sizes
    StrategyList_t GL;         // list of strategies
    Sizes vL;                  // list of vacancies for
                               // each strategy.

    // Constructor
    Strategies(sz_t z, const Sizes& sL);

    // Base case result initializers
    StrategyList_t initGL();
    StrategyList_t emptyGL();

    // Compute wrapper (handy for memoization)
    void compute();

    // Workhorse
    StrategyList_t _compute(sz_t i, sz_t x);

    // Memoized version (workhorse)
    std::function< StrategyList_t(sz_t, sz_t) >
    memoized_compute;
    
  };
}
