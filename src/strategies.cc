/**
 * Given bag size `z', and list of article sizes `s_i',
 * compute strategies `g_j' such that
 * `residual < min(s_i); residual = z - sum(g_j)'
 *
 */

#include <iterator>
#include <algorithm>
#include <functional>
using namespace std;

#include <boost/range/irange.hpp>
using boost::irange;

#include <glog/logging.h>

#include <yackonop/strategies.hpp>
#include <yackonop/memoize.hpp>

namespace yackonop {

  Strategies::Strategies(sz_t z, const Sizes& sL)
    : z(z), sL(sL) {
    
    memoized_compute = memoize(
      std::function< StrategyList_t(sz_t, sz_t) >(
        bind(&Strategies::_compute,
             this,
             std::placeholders::_1,
             std::placeholders::_2)
        ));

  }

  StrategyList_t Strategies::initGL() {
    return StrategyList_t(1, Strategy_t(sL.size(), 0));
  }

  StrategyList_t Strategies::emptyGL() {
    return StrategyList_t(0, Strategy_t(sL.size(), 0));
  }

  StrategyList_t
  Strategies::_compute(sz_t i, sz_t x) {

    /* Initialize result */
    auto GL = emptyGL(); 

    /* Handle the base case
     */
    if (i >= sL.size()) {
      VLOG(1) << "Strategies::_compute: "
              << "terminal condition: i: "
              << i << " N: " << sL.size()
              << " x: " << x
              << endl;

      if (x < *min_element(sL.begin(), sL.end())) {
        VLOG(1) << "Strategies::_compute: "
                << "Path valid." << endl;


        /* Valid path. Initialize path to be
         * populated */
         GL = initGL();           // zero-valued single
                                  // item set.
         GL.front().push_back(x); // G[N] = wastage
                                  // = z - dot(G[:N], S)
      }

      return GL;
    }


    for (auto k : irange(1+ x / sL[i])) {
      /* Copy state */
      auto y = x;               // copy available capacity

      VLOG(4) << "Strategies::_compute: i: "  << i
              << " y: " << y << " k: " << k
              << " sL[i]: " << sL[i] << " y/sL[i]: "
              << y/sL[i] << endl;

      /* Put k-copies of article in the bag. Recompute
       * the available capacity */
      y -= k * sL[i];

      /* Invalidite the progression */
      if (y < 0) {              // impossible strategy
        VLOG(1) << "Strategies::_compute: "
                << "impossible strategy:"
                << " y: " << y << endl;
        
        break;
      }

      /* Compute strategies by recursion */
      auto HL = memoized_compute(1+i, y);

      VLOG(4) << "Strategies::_compute: "
              << "got strategies: " << HL
              << endl;

      /* Fill into the strategies */
      transform(
        HL.begin(), HL.end(), HL.begin(),
        [=](auto& H) { H[i] = k; return H; }
        );

      VLOG(4) << "Strategies::_compute: "
              << "transformed strategies"
              << "(i:" << i << ",k:" << k << "): "
              << HL << endl;

      /* Move the relevant strategies to result */
      VLOG(2) << "Strategies::_compute: "
              << "Copying to GL (size: "
              << GL.size() << ")";
      // copy(HL.begin(), HL.end(), GL.end());
      GL.insert(GL.end(), HL.begin(), HL.end());
      VLOG(2) << " ...done" << endl;
      
    }
    
    return GL;
  }

  void Strategies::compute() {
     GL = memoized_compute(0, z);

     VLOG(1) << "Strategies::compute: GL: "
             << GL << endl;

     if (!GL.empty()) transform( // there are
                                 // strategies.
       GL.begin(), GL.end(), back_inserter(vL),

       // Remove the last element: G[N], and store in a
       // separate vacancies vector \forall G \in GL
       [](auto& G) mutable {
         auto tmp = G.back() ; G.pop_back(); return tmp;
       });
  }

}


// Local Variables:
// comment-auto-fill-only-comments: t
// End:
