#include <boost/range/irange.hpp>
using boost::irange;
#include <boost/range/combine.hpp>
using boost::combine;

#include <functional>
#include <tuple>
#include <algorithm>
using namespace std;

#include <yackonop/types.hpp>
#include <yackonop/memoize.hpp>
#include <yackonop/solver.hpp>

namespace yackonop {

  const count_t Solver::INFTY =
    static_cast<count_t>(-1);

  Solver::Solver(const sz_t z,
                 const Sizes S,
                 const Counts c,
                 const StrategyList_t& G)
    : z(z), S(S), c(c), G(G) {

    memoized_min_vacancy = memoize(
      function<
        tuple<count_t, Counts>(Counts , count_t)
      >(bind(
          &Solver::_min_vacancy, this,
          placeholders::_1, placeholders::_2
          )));

    memoized_vacancy = memoize(
      function<
        tuple<count_t, Counts>(Counts , count_t, count_t)
      >(bind(
        &Solver::_vacancy, this,
        placeholders::_1, placeholders::_2,
        placeholders::_3
          )));

  }

  template<typename T, typename U>
  T maxceildiv(vector<T> dividends, vector<U> divisors) {
    vector<T> tmp;

    VLOG(4) << "maxceildiv: dividends: " << dividends;
    VLOG(4) << "maxceildiv: divisors: "  << divisors;

    transform(
      dividends.begin(), dividends.end(),
      divisors.begin(), back_inserter(tmp),
      [](auto a, auto b) {
        return b == 0 ? b
          : a / b + (a % b != 0);
      });

    T result = *max_element(
      tmp.begin(), tmp.end());

    VLOG(4) << "maxceildiv: result: " << result;

    return result;
  }

  /**
   * _min_vacancy
   * -----------------------------------
   */
  tuple<count_t, Counts>
  Solver::_min_vacancy(Counts c, count_t k) {
    VLOG(2) << "_min_vacancy: k: " << k
            << " c: " << c;

    // Handle the base case
    if (k >= G.size()) {        // terminal condition
      VLOG(2) << "_min_vacancy: Terminal condition"
              << " reached.";

      count_t v = INFTY;
      auto sr = Counts(0);

      auto d = Counts();
      transform(c.begin(), c.end(), back_inserter(d),
                [](auto i) { return i<=0; });
      VLOG(4) << "_min_vacancy: (c <= 0): " << d;

      if (all_of(c.begin(), c.end(),
                 [](auto i) { return i<=0; })) {
        // This set of strategies satisfies the
        // constraint.
        v = 0;
        VLOG(2) << "_min_vacancy: All zeros";
      }

      return make_tuple(v, sr);
    }

    VLOG(2) << "_min_vacancy: " << "k: " << k
            << " c: " << c;

    // vacancies state
    count_t v_star, v, h, i_star=0;
    // strategy repeats bookkeeping
    Counts sr, sr_star;

    // Initialize optimum v to infinity
    v_star=INFTY;

    // count of branches (or guess)
    h = maxceildiv(c, G[k]);
    VLOG(2) << "_min_vacancy: h: " << h << " k: " << k
            << " c: " << c << " G[k]: " << G[k];
    VLOG(2) << "================";


    for (auto i : irange(1+h)) {
      VLOG(2) << "----------------";
      VLOG(2) << "_min_vacancy: i: " << i
              << " k: " << k << " c: " << c
              << " G[k]: " << G[k];

      // recurse (or map)
      tie(v, sr) = memoized_vacancy(c, k, i);

      VLOG(4) << "_min_vacancy: " << "(c:" << c
              << ",k:" << k << ",i:" << i
              << ") => (v:" << v << ",sr:" << sr << ")";

      // combine (or reduce)
      if (v < v_star) i_star=i, v_star=v, sr_star=sr;

    }

    // consolidate
    VLOG(4) << "_min_vacancy: sr_star: " << sr_star
            << " i_star: " << i_star;
    sr_star.insert(sr_star.begin(), i_star);
    VLOG(4) << "_min_vacancy: sr_star: " << sr_star;

    // return
    return make_tuple(v_star, sr_star);
    
  }

  /**
   * _vacancy
   * -----------------------------------
   */
  tuple<count_t, Counts>
  Solver::_vacancy(Counts c, count_t k, const count_t i) {
    count_t t, u = 0;           // utilities

    VLOG(4) << "_vacancy: c: " << c << " k: " << k
            << " i: " << i << " G[k]: " << G[k];
    // compute c less i times strats
    for (auto j : irange (c.size())) {
      count_t& c_j = c[j];
      const sz_t g_kj = G[k][j];
      const sz_t s_j = S[j];

      // compute utilities
      t = min(
        max(0u, c_j),           // negative utilities
                                // are not desired
        i * g_kj
        );

      // trade off the utilites
      c_j -= t, u += t * s_j;
    }
    VLOG(4) << "_vacancy: c: " << c;

    auto [v, sr] = memoized_min_vacancy(c, ++k);
    VLOG(4) << "_vacancy: _min_vancancy(c:" << c
            << ",k:" << k << ") => (v:" << v
            << ",sr:" << sr << ")";

    if (v < INFTY) v += i * z - u;
    VLOG(4) << "_vacancy: v:" << v;

    return make_tuple(v, sr);
  }

  /**
   * solve
   * -----------------------------------
   */
  tuple<count_t, Counts>
  Solver::solve() {
    return memoized_min_vacancy(c, 0);
  }  


}  // yackonop

// Local Variables:
// comment-auto-fill-only-comments: t
// End:
