#include <glog/logging.h>

#include <iostream>
#include <tuple>
using namespace std;

#include <boost/range/irange.hpp>
using boost::irange;

#include <yackonop/types.hpp>
namespace yackonop {

  template<typename T>
  vector<T> read_vector(
    vector<T> &vec , istream& strm, size_t sz
    ) {

    typedef istream_iterator<T> read_it;

    // The following function (in conjunction with
    // copy_if) does not inhibit read from stream. Just
    // that it inhibits copy. Which makes the function
    // useless.

    // auto counter_alive = [count=static_cast<int>(sz)](T tmp) mutable {
    //   LOG(INFO) << "parse_input: parse_input: count: "
    //             << count << " tmp: " << tmp << endl;
    //   return --count >= 0;
    // };

    // copy_if(read_it(strm), read_it(),
    //         back_inserter(vec), counter_alive);


    copy_n(read_it(strm), sz, back_inserter(vec));

    return vec;
  }

  tuple<sz_t, Sizes, Counts>
  parse_input(istream& strm) {

    sz_t bag_size, num_items;
    auto sizes = Sizes ();
    auto counts = Counts ();

    strm >> bag_size;
    strm >> num_items;
    read_vector(sizes, strm, num_items);
    read_vector(counts, strm, num_items);

    return make_tuple(bag_size, sizes, counts);
  }

}
