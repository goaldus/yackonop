#include <boost/range/irange.hpp>
using boost::irange;
#include <boost/range/combine.hpp>
using boost::combine;

#include <tuple>
using namespace std;

#include <yackonop/types.hpp>
#include <yackonop/parse_input.hpp>
#include <yackonop/strategies.hpp>
#include <yackonop/solver.hpp>
using namespace yackonop;

#include <prettyprint.hpp>

#include <glog/logging.h>

int main(int argc, char *argv[])
{
  FLAGS_logtostderr = true;
  FLAGS_stderrthreshold = 0;
  google::InitGoogleLogging(argv[0]);

  sz_t bag_size;
  Sizes sizes;
  Counts counts;
  
  tie(bag_size, sizes, counts) = parse_input(cin);

  LOG(INFO) << "yackonop: main: bag_size: "
            << bag_size << endl;
  LOG(INFO) << "yackonop: main:  sizes: "
            << sizes << endl;
  LOG(INFO) << "yackonop: main:  counts: "
            << counts << endl;

  Strategies st(bag_size, sizes);
  st.compute();

  LOG(INFO) << "yackonop: main: num_strategies: "
            << st.GL.size() << endl;

  LOG(INFO) << "yackonop: main: strategies: "
            << st.GL << endl;

  count_t vacancies;
  Counts st_mask;

  Solver solver(bag_size, sizes, counts, st.GL);
  tie(vacancies, st_mask) = solver.solve();

  LOG(INFO) << "yackonop: main: vacancies: "
            << vacancies << endl;
  LOG(INFO) << "yackonop: main: strategy_mask: "
            << st_mask << endl;

  return 0;
}
