# Yackonop #

**Yet another variant of Knapsack-problem based
constrained optimization problem**

## The problem ##

Given a set of articles, say $`N`$ types, each with a
given size $`s_i`$, and count $`a_i`$ in the set, and
given the bag capacity $`z`$, What is the minimum
number of bags required to fill all the articles.

### Sample Input ###

The input is read from `STDIN` with following entries,
in each line. A list is space delimited to be written
in a line.

- Bag capacity $`z ; z<2^{16} \in \N`$
- Number of article types $`N ; N<2^{16} \in \N`$
- Size of each article, $`\{s_i: s_i<z, i \leqslant N
  \in \N ; s_i \cancel= s_j \forall i\cancel= j\}`$
- Numbers of each article available, $`\{a_i:
  a_i<2^{32}, i\leqslant N \in \N; \sum_i a_i < 2^{32}
  \}`$

A sample input (as in [cases/1](./cases/1)):
```
96
13
6 12 18 24 30 32 36 40 48 60 72 80 84
106 26 50 112 39 9 56 73 64 96 152 101 112
```

## Use of memoization ##
For the sample problem in [`case 1`](./cases/1), the
`Strategies::compute` was invoked $`\sim 600`$ times,
of which $`\sim 225`$ times there was a recall. Further
the graph traversal had to hit the sentinel i.e. reach
the terminal condition, only $`38`$ times, of which the
ones that resulted in optimal paths were only $`3`$. To
summarise, the combination strategies for a 12-class
item list were computed using only $`3`$ optimal
sentinel paths.

Here are the run time metrics for the same case with
items arranged in ascending vs. descending order.
```
asc/memoize      : 605         desc/memoize      : 547
asc/recall       : 223         desc/recall       : 337
asc/impossible   : 0           desc/impossible   : 0  
asc/terminal     : 38          desc/terminal     : 38
asc/'Path valid' : 3           desc/'Path valid' : 3
```

## TODO ##

At commit b98c7ad: The program comes to a halt, but
does not compute properly. The algorithm reaches the
terminal condition only twice, (with or without
memoization) and they are not correct terminal
conditions... So there seems to be some problem.
