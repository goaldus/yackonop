#include <iostream>
#include <tuple>
using namespace std;

#include <yackonop/types.hpp>
#include <yackonop/parse_input.hpp>
#include <yackonop/strategies.hpp>
using namespace yackonop;

#include <prettyprint.hpp>

#include <glog/logging.h>

int main(int argc, char *argv[])
{
  FLAGS_logtostderr = true;
  FLAGS_stderrthreshold = 0;
  google::InitGoogleLogging(argv[0]);

  sz_t bag_size;
  Sizes sizes;
  Counts counts;
  
  tie(bag_size, sizes, counts) = parse_input(cin);

  LOG(INFO) << "test/strategies: main: bag_size: "
            << bag_size << endl;
  LOG(INFO) << "test/strategies: main:  sizes: "
            << sizes << endl;
  LOG(INFO) << "test/strategies: main:  counts: "
            << counts << endl;

  Strategies st(bag_size, sizes);
  st.compute();

  LOG(INFO) << "test/strategies: main: num_strategies: "
            << st.GL.size() << endl;

  return 0;
}
